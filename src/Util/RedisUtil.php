<?php

declare(strict_types=1);

namespace App\Util;

use App\Exception\CacheException;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\TraceableAdapter;

class RedisUtil implements CacheInterface
{
    public function saveItem(TraceableAdapter $cachePool, string $key, string $value): bool
    {
        $item = $this->fetch($cachePool, $key);
        $item->set($value);

        return $cachePool->save($item);
    }

    public function getItem(TraceableAdapter $cachePool, string $key): ?string
    {
        $result = null;

        $item = $this->fetch($cachePool, $key);
        if ($item->isHit()) {
            $result = $item->get();
        }

        return $result;
    }

    public function deleteItem(TraceableAdapter $cachePool, string $key): bool
    {
        return $cachePool->deleteItem($key);
    }

    public function deleteAll(TraceableAdapter $cachePool): bool
    {
        return $cachePool->clear();
    }

    private function fetch(TraceableAdapter $cachePool, string $key): CacheItemInterface
    {
        try {
            return $cachePool->getItem($key);
        } catch (InvalidArgumentException $e) {
            throw new CacheException($e->getMessage());
        }
    }
}