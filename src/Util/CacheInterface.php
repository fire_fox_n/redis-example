<?php
declare(strict_types=1);

namespace App\Util;

use Symfony\Component\Cache\Adapter\TraceableAdapter;

interface CacheInterface
{
    public function saveItem(TraceableAdapter $cachePool, string $key, string $value): bool;

    public function getItem(TraceableAdapter $cachePool, string $key): ?string;

    public function deleteItem(TraceableAdapter $cachePool, string $key): bool;

    public function deleteAll(TraceableAdapter $cachePool): bool;
}