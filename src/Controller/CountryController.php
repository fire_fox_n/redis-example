<?php
declare(strict_types=1);

namespace App\Controller;

use App\Util\CacheInterface;
use Symfony\Component\Cache\Adapter\TraceableAdapter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/countries")
 */
class CountryController
{
    private $cachePool;
    private $cacheUtil;

    public function __construct(
        CacheInterface $cacheUtil,
        TraceableAdapter $cachePool
    ) {
        $this->cachePool = $cachePool;
        $this->cacheUtil = $cacheUtil;
    }

    /**
     * @Route("/save-item", methods={"GET"})
     */
    public function saveItem(): JsonResponse
    {
        return new JsonResponse($this->cacheUtil->saveItem($this->cachePool, 'key', 'country'));
    }

    /**
     * @Route("/get-item", methods={"GET"})
     */
    public function getItem(): JsonResponse
    {
        return new JsonResponse($this->cacheUtil->getItem($this->cachePool, 'key'));
    }

    /**
     * @Route("/delete-item", methods={"GET"})
     */
    public function deleteItem(): JsonResponse
    {
        return new JsonResponse($this->cacheUtil->deleteItem($this->cachePool, 'key'));
    }

    /**
     * @Route("/delete-all", methods={"GET"})
     */
    public function deleteAll(): JsonResponse
    {
        return new JsonResponse($this->cacheUtil->deleteAll($this->cachePool));
    }
}